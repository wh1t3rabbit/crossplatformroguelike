﻿using System;
namespace crossplatformroguelike.Game
{
	internal class Game
	{
		public static string __version = "build 201608112122";
		public GameState state;
		private int bufW;
		private int bufH;
		private string lastKey;
		public Game()
		{
			state = GameState.Title;
			bufW = -1;
			bufH = -1;
			lastKey = "NONE";
		}

		internal void ProcessKey(ConsoleKeyInfo keyinfo, out bool quit)
		{
			quit = keyinfo.Key.Equals(ConsoleKey.Escape);

			lastKey = keyinfo.Key.ToString()+" ["+keyinfo.KeyChar+"]";

			if (!quit)
			{
				switch (state)
				{
					case GameState.Title:
						break;
					case GameState.Options:
						break;
					case GameState.Play:
						break;
					case GameState.Shop:
						break;
					case GameState.GameOver:
						break;
					case GameState.Credits:
						break;
				}
			}
		}

		internal void Render()
		{
			
			int cWidth = Console.BufferWidth<Console.WindowWidth ? Console.BufferWidth : Console.WindowWidth;
			int cHeight = Console.WindowHeight;
			bool changed = cWidth != bufW || cHeight != bufH;
			bufW = cWidth;
			bufH = cHeight;
			if (cHeight == 0) { return; } //incase we resize the window too small

			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.White;
			Console.SetWindowPosition(0, 0);

			if (changed) {
				Console.Clear();
			}

			//Set buffer size to window size to hide vertical scrollbar
			//Console.SetBufferSize(cWidth,cHeight); //Disabled, results in screen flickering because the scrollbar keeps flashing up on input - ALSO prevents resizing the window larger after going smaller

			if (!RenderSanityCheck(cWidth, cHeight)) { return; }


			if (changed)
			{
				//Everything was just cleared, redraw the borders and title

				//Borders
				string horizontalBorder = new string('-', cWidth - 2);
				Console.BackgroundColor = ConsoleColor.Black;
				Console.ForegroundColor = ConsoleColor.Gray;
				//Top border
				Console.SetCursorPosition(1, 1);
				Console.Write(horizontalBorder);
				//Bottom border
				Console.SetCursorPosition(1, cHeight - 3);
				Console.Write(horizontalBorder);
				//Side borders
				for (int i = 2, n = cHeight - 3; i < n; i++)
				{
					Console.SetCursorPosition(1, i);
					Console.Write("|");
					Console.SetCursorPosition(cWidth - 2, i);
					Console.Write("|");
				}

			}

			//Clear top line
			ClearRegion(1, 0, cWidth - 3, 1, false);

			//Version at top
			string title = "crossplatformroguelike " + __version;
			Console.SetCursorPosition(1, 0);
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.Write(title);

			//Update draw area size display above border
			int linesAvail = cHeight - 5;
			int colsAvail = cWidth - 4;
			string sizestr = "Inner Area " + colsAvail + " x " + linesAvail;
			int sizepos = cWidth / 2 - sizestr.Length / 2;
			sizepos = sizepos < 0 ? 0 : sizepos;
			Console.SetCursorPosition(sizepos, 0);
			Console.ForegroundColor = ConsoleColor.DarkGreen;
			Console.Write(sizestr);

			//Update time display above border
			string timestr = DateTime.Now.ToLongTimeString();
			int timepos = cWidth - 1 - timestr.Length;
			timepos = timepos < 0 ? 0 : timepos;
			Console.SetCursorPosition(timepos, 0);
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.Write(timestr);

			//Clear inside the borders
			ClearRegion(1, 1, cWidth - 3, cHeight - 5, true);

			//Draw current state inside the borders
			switch (state)
			{
				case GameState.Title:
					Console.SetCursorPosition(3, 3);
					Console.BackgroundColor = ConsoleColor.DarkBlue;
					Console.ForegroundColor = ConsoleColor.Yellow;
					Console.Write("Key: "+lastKey);
					break;
				case GameState.Options:
					break;
				case GameState.Play:
					break;
				case GameState.Shop:
					break;
				case GameState.GameOver:
					break;
				case GameState.Credits:
					break;
			}

			//Update input line below border
			ClearRegion( 1, cHeight-2, cWidth-3, 1, false );
			Console.SetCursorPosition(1, cHeight - 2);
			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.Write("Input: ");
		}
		//Incase we resize the window really small
		internal bool RenderSanityCheck(int cWidth,int cHeight)
		{
			if (cHeight <= 3)
			{
				Console.ForegroundColor = ConsoleColor.Magenta;
				Console.SetCursorPosition(0, 0);
				Console.Write("Too small");
				if (cHeight > 1)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.SetCursorPosition(0, 1);
					Console.Write("wtfsrsly");
				}
				return false;
			}
			return true;
		}

		internal void ClearRegion(crossplatformroguelike.Game.Util.Rectangle r,bool insideOnly=false)
		{
			ClearRegion(r.X,r.Y,r.W,r.H,insideOnly);
		}

		void ClearRegion(int x, int y, int w, int h,bool insideOnly=false)
		{
			int minX = x;
			int maxX = minX + w;
			int minY = y;
			int maxY = minY + h;
			if (insideOnly)
			{
				minX++;
				maxX--;
				minY++;
				maxY++;
			}
			if (minX < 0) { minX = 0; }
			if (maxX > bufW - 1) { maxX = bufW - 1; }
			if (minY < 0) { minY = 0; }
			if (maxY > bufH - 1) { maxY = bufH - 1; }
			int sW = maxX - minX + 1;
			if (sW <= 0) { return; }
			int lineC = maxY - minY;
			if (lineC <= 0) { return; }
			string f = new string(' ', sW);
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.White;
			for (int p = minY; p < maxY; p++)
			{
				Console.SetCursorPosition(minX,p);
				Console.Write(f);
			}
		}
}
	internal enum GameState
	{
		Title,
		Options,
		Play,
		Shop,
		GameOver,
		Credits
	}
}

