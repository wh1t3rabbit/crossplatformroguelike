﻿using System;
using crossplatformroguelike.Game.Util;
namespace crossplatformroguelike.Game
{
	internal interface IDrawable
	{
		void Draw(Rectangle withinBounds);
	}
}

