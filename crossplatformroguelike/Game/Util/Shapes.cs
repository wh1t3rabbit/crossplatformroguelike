﻿using System;
namespace crossplatformroguelike.Game.Util
{

	#region Class Rectangle
	internal class Rectangle
	{
		#region Properties
		private int x;
		public int X
		{
			get
			{
				return x;
			}
			set
			{
				x = value;
			}
		}
		private int y;
		public int Y
		{
			get
			{
				return y;
			}
			set
			{
				y = value;
			}
		}
		private int w;
		public int W
		{
			get
			{
				return w;
			}
			set
			{
				w = value;
			}
		}
		private int h;
		public int H
		{
			get
			{
				return h;
			}
			set
			{
				h = value;
			}
		}
		#endregion Properties
		#region ReadOnly properties
		public Point2D GetPos()
		{
			return new Point2D(x,y);
		}
		#endregion ReadOnly Properties
		#region Constructor
		public Rectangle() : this(0, 0, 0, 0) {}
		public Rectangle(int x, int y, int w, int h)
		{
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}
		#endregion Constructor
	}
	#endregion Class Rectangle

	#region Class Point2D
	internal class Point2D
	{
		#region Properties
		private int x;
		public int X
		{
			get
			{
				return x;
			}
			set
			{
				x = value;
			}
		}
		private int y;
		public int Y
		{
			get
			{
				return y;
			}
			set
			{
				y = value;
			}
		}
		#endregion Properties
		#region Constructor
		public Point2D(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
		#endregion Constructor
		#region Methods
		public int Distance(Point2D p2)
		{
			return (int)Math.Sqrt((p2.x - x) * (p2.x - x) + (p2.y - y) * (p2.y - y));
		}
		public int DistanceSq(Point2D p2)
		{
			return (p2.x - x) * (p2.x - x) + (p2.y - y) * (p2.y - y);
		}
		#endregion Methods
	}
	#endregion Class Point2D

}

