﻿using System;

namespace crossplatformroguelike
{
	class Program
	{
		
		public static void Main(string[] args)
		{
			//Clear on startup
			Console.Clear();
			//Launch game
			Program instance = new Program();
			instance.run();
			//Clear on shutdown
			Console.Clear();
			Console.SetCursorPosition(0,0);
		}

		readonly Game.Game game;
		public Program()
		{
			game = new Game.Game();
		}

		private void run()
		{
			
			bool quit = false;
			while (!quit)
			{
				game.Render();
				game.ProcessKey( Console.ReadKey(true), out quit );
			}

			Console.Write("Quitted; Press any key to exit...");
			Console.ReadKey(true);
		}

	}
}
